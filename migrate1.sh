# handle () {
#   commands
# }

# for name in "analyticsb2b" "brandaid" "cdi" "code2success" "kassandra" "knowledge-graph" "kolumbus" "onair-interdependency-analysis" "onair-reach-prediction"	"panelytics" "product42" "signaturecode" "vipa"	 
for name in "analyticsb2b"
do
    env=dev
    state_bucket_name="" # is filled later
    state_bucket_name_terraform="mgr-$name-product-tfstate-$env"
    tfstate_config_file="terraform/$env/.terraform/terraform.tfstate"
    bkp_dir=statefiles/bkp/$name
    echo "" > "statefiles/$name.json"
    mkdir $bkp_dir

    # ___k8s-infra__

    # cp -r terraformcache/k8s-infra/.terraform ./k8s-infra/terraform/$env
    # cd k8s-infra

    # git remote rm origin
    # git remote add origin "git@gitlab.com:dataalliance/products/$name/infra/k8s-infra.git"
    # git fetch
    # git checkout main
    # git reset --hard origin/main
    # git switch -C main origin/main
    # git checkout feature/DATOOLS-851-migrate-airflow
    # prefix="terraform/k8s-infra/dev"
    # state_bucket_name=$(magic -e $env --config-remote-force-sync --config-remote-ref 7.4.0 config show | jq -r ".terraform.state_bucket.name")
    # cat <<< "$(jq --arg state_bucket_name $state_bucket_name --arg prefix $prefix '.backend.config.bucket=$state_bucket_name | .backend.config.prefix=$prefix' $tfstate_config_file)" > $tfstate_config_file
    # cd terraform/$env
    # # terraform state pull > ../../../$bkp_dir/k8s-infra.json
    # terraform state pull > ../../../statefiles/${name}.json
    # # terraform state mv --state-out="../../../statefiles/${name}.json" module.airflow_base_infra_main.module.airflow_wlid.google_service_account_iam_member.wlid
    # echo $state_bucket_name
    # cd ../../..


    # __infra/base-infra__
    # rm -f base-infra/terraform/common/.terraform.lock.hcl
    # rm -f base-infra/terraform/$env/.terraform.lock.hcl
    # rm -rf base-infra/terraform/$env/.terraform
    # # cp -r terraformcache/base-infra/.terraform ./base-infra/terraform/$env
    # cd base-infra
    # git remote rm origin
    # git remote add origin "git@gitlab.com:dataalliance/products/$name/infra/base-infra.git"
    # git fetch
    # git checkout main
    # git reset --hard origin/main
    # git switch -C main origin/main
    # prefix="terraform/state/dev"
    # cat <<< "$(jq --arg state_bucket_name $state_bucket_name --arg prefix $prefix '.backend.config.bucket=$state_bucket_name | .backend.config.prefix=$prefix' $tfstate_config_file)" > $tfstate_config_file
    # sed -i 's/name: mgr-main-${resource_suffix}//' extra-context/common.yaml
    # magic -e $env -c extra-context terraform init
    # cd terraform/$env
    # terraform state pull > ../../../$bkp_dir/base-infra.json

    # terraform state mv --state-out="../../../statefiles/$name" module.airflow_base_infra_main module.airflow_base_infra_main 
    
    # cd ../../..

    # product/terraform__
    # rm -f terraform/terraform/common/.terraform.lock.hcl
    # rm -f terraform/terraform/$env/.terraform.lock.hcl
    # rm -rf terraform/terraform/$env/.terraform
    cd terraform
    git remote rm origin
    git remote add origin "git@gitlab.com:dataalliance/products/$name/product/terraform.git"
    git fetch
    git checkout main
    git reset --hard origin/main
    git switch -C main origin/main
    export prefix="terraform/state/dev"
    cat <<< "$(jq --arg state_bucket_name $state_bucket_name_terraform --arg prefix $prefix '.backend.config.bucket=$state_bucket_name | .backend.config.prefix=$prefix' $tfstate_config_file)" > $tfstate_config_file
    sed -i 's/name: mgr-main-${resource_suffix}//' extra-context/common.yaml
    # terraform providers lock -platform=windows_amd64 -platform=darwin_amd64 -platform=linux_amd64
    magic -e $env -c extra-context terraform init
    cd terraform/$env
    # terraform state pull > ../../../$bkp_dir/terraform.json
    terraform state mv --state-out="../../../statefiles/$name" module.airflow.kubectl_manifest.generated[\".terraform/modules/airflow/manifests/Application.yaml\"] module.airflow_base_infra_main.kubectl_manifest.generated[\".terraform/modules/airflow_base_infra_main/manifests/Application.yaml\"]
    terraform state mv --state-out="../../../statefiles/$name" module.airflow.module.sql_credentials module.airflow_base_infra_main.module.sql_credentials
    # terraform state rm --state-out="../../../statefiles/$name" module.airflow.module.sql_credentials module.airflow_base_infra_main.module.sql_credentials

    cd ../../..
done